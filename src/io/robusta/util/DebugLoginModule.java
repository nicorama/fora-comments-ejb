package io.robusta.util;

import javax.security.auth.login.LoginException;

import org.jboss.security.auth.spi.DatabaseServerLoginModule;
import org.jboss.security.auth.spi.Util;

public class DebugLoginModule extends DatabaseServerLoginModule {

	 protected String[] getUsernameAndPassword() throws LoginException{
		 
		 String[] result = super.getUsernameAndPassword();
		 log.info(">>>> username : "+result[0]);
		 log.info(">>>> password : "+result[1]);
		 return result;
	 }
	 
	 public boolean login() throws LoginException{
		 log.info(">>>> trying to log in");
		 return super.login();
	 }

	 
	 protected boolean validatePassword(String inputPassword, String expectedPassword){
		 
		 log.info(">>>> input password : "+inputPassword);
		 log.info(">>>> expected password : "+expectedPassword);
		 boolean result = super.validatePassword(inputPassword, expectedPassword);
		 return result;
	 }
	 
	 protected String createPasswordHash(String username, String password,
		     String digestOption)
		     throws LoginException{
		 
		 String passwordHash = super.createPasswordHash(username, password, digestOption);
		 log.info(">>>>> passwordHash: "+passwordHash );
		 return passwordHash;
	 }
	 
	 public static String createPasswordSHAHash(String password){
		 return Util.createPasswordHash("SHA-256", Util.BASE64_ENCODING, null, null, password);
	 }
	 public static String createPasswordSHAHash(String username, String password){
		 return Util.createPasswordHash("SHA-256", Util.BASE64_ENCODING, null, username, password);
	 }
	 
	 @Override
	protected String convertRawPassword(String rawPassword) {
		 log.info(">>>>> raw password : "+rawPassword);
		return super.convertRawPassword(rawPassword);
	}
	 
	 public static void main (String[] args){
		 System.out.println(Util.createPasswordHash("SHA-256", Util.BASE64_ENCODING, null, "Nicolas", "123123"));
		 System.out.println(createPasswordSHAHash("Nicolas", "123123"));
		// System.out.println(createPasswordSHAHash(null, null));
	 }
	 
}
