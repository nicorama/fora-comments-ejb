package io.robusta.fora.resteasy;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;


import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("cache2")
public class WsCache {

	
	@GET
	@Produces("application/json")
	public Response expires(){
		
		System.out.println("Expires in 365 days");
		String tommorow = getDaysAfterHttpDate(365);
		return Response.ok("{Hello:Hello}").header("Expires",tommorow).build();
	}
		
	
	
	String getDaysAfterHttpDate(int days) {
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTime(new Date()); // Now use today date.
	    calendar.add(Calendar.DATE, days); // adding days
	    SimpleDateFormat dateFormat = new SimpleDateFormat(
	        "EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
	    dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));	    	    
	    return dateFormat.format(calendar.getTime());
	}
}
