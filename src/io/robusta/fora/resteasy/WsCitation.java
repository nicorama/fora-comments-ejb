package io.robusta.fora.resteasy;

import io.robusta.fora.business.CitationEjb;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("citation")
public class WsCitation {

	@EJB CitationEjb bean;
	@GET
	public String getCitation(){
		return bean.getCitation();
	}
}
