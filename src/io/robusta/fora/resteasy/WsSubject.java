package io.robusta.fora.resteasy;

import io.robusta.fora.beans.SubjectBean;
import io.robusta.fora.business.SubjectBusiness;
import io.robusta.fora.business.UserBusiness;
import io.robusta.fora.domain.Comment;
import io.robusta.fora.domain.Subject;
import io.robusta.fora.domain.User;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

@Path("subject")
public class WsSubject{


	@EJB SubjectBusiness subjectBusiness;
	@EJB UserBusiness userBusiness;
	
	@GET 
	@Path("/domain/{subjectId}")
	@Produces (MediaType.APPLICATION_JSON)
	public Subject  getDomainSubject(@PathParam("subjectId") long id) throws IOException	{
			if (id <1 ){
				throw new RuntimeException("Bad id");
			}	
			
			Subject s = subjectBusiness.getSubjectById(id);
			
			if (s == null){
				throw new RuntimeException("cant find subject num : "+id);
			}
			else{
				return s;
			}
			
	}

	@GET 
	@Path("{subjectId}")
	@Produces (MediaType.APPLICATION_JSON)
	public SubjectBean  getSubject(@PathParam("subjectId") long id) throws IOException	{
			if (id <1 ){
				throw new RuntimeException("Bad id");
			}
		
			SubjectBean bean = subjectBusiness.getSubjectBean(id);
			//Subject s = subjectBusiness.getSubjectById(id);
			
			if (bean == null){
				throw new RuntimeException("cant find subject num : "+id);
			}
			else{
			    
				return bean;
			}
			
	}
	
	
	@GET 
	@Path("/all")
	@Produces (MediaType.APPLICATION_JSON)
	public List<Subject>  getAllSubjects() throws IOException	{
					
			return subjectBusiness.getAllSubjects();
			
	}
	
	/*@GET 
	@Path("{subjectId}")
	@Produces (MediaType.APPLICATION_JSON)
	public Subject getSubject(@PathParam("subjectId") long id)	{
			if (id <1 ){
				throw new RuntimeException("Bad id");
			}
		
			
			Subject s = subjectBusiness.getSubjectById(id);
			
			if (s == null){
				throw new RuntimeException("cant find subject num : "+id);
			}
			else{
				return s;
			}
	}*/



	
	@GET 
	@Path("{subjectId}/comments")
	@Produces(MediaType.APPLICATION_XML)
	public List<Comment> getComments(	@PathParam("subjectId") long id){
		//Subject s = em.find(Subject.class, id);
		//return s.getComments();
		return null;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{subjectId}/comment")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response createComment(
			@PathParam("subjectId") long id,
			@FormParam("content") String content,
			@FormParam("userId") long userId,
			@FormParam("anonymous") boolean anonymous
			){
		
		Comment c;
		
		//checking subject
		Subject s = subjectBusiness.getSubjectById(id);
		if (s == null){
			return notAcceptable("Can't find subject "+id);
		}
		
		//checking content
		if (content == null || content.isEmpty()){
			return notAcceptable("No content for comment");
		}
		
		//checking user
		if (anonymous){
			c = new Comment();
			c.setAnonymous(true);
			c.setContent(content);
		}else{
			User u = null;
		/*	User u = userBusiness.getUser(userId);
			if (u == null){
				return Response.status(Status.NOT_ACCEPTABLE).entity("Can't find user "+userId).build();
			}*/
			c = new Comment(u, content);
		}		
		
		String commentId = subjectBusiness.addComment(s, c);
		return ok(commentId);
		
		
	}
	
	private Response ok(Object content){
		return Response.ok(content).build();
	}
	
	private Response notAcceptable(String reason){
		return Response.status(Status.NOT_ACCEPTABLE).entity(reason).build();
	}
}
