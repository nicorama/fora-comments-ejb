package io.robusta.fora.business;

import io.robusta.fora.beans.SubjectBean;
import io.robusta.fora.domain.Comment;
import io.robusta.fora.domain.Flag;
import io.robusta.fora.domain.Subject;
import io.robusta.fora.util.ForaLogger;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

@Stateless
public class SubjectBusiness {

	// @EJB ForaDataSource datasource;
	@Inject
	ForaLogger logger;

	@PersistenceContext
	EntityManager em;


	public Subject getSubjectById(long id) {
		
		TypedQuery<Subject> tQuery = 
				em.createQuery("Select s from Subject s JOIN FETCH s.comments AS comments WHERE s.id = :id",
						Subject.class);
		tQuery.setParameter("id", id);
		
		Subject s = tQuery.getSingleResult();
		
		
		
		System.out.println("My subject has "+s.getComments().size()+ " comments");
		return s;
		
	}

	public SubjectBean getSubjectBean(long id) {
		Subject s = getSubjectById(id);
		SubjectBean bean = new SubjectBean(s);
		return bean;
	}

	public List<Subject> getAllSubjects() {
		Query query = em
				.createQuery("Select s from Subject s JOIN FETCH s.comments");
		List<Subject> subjects = query.getResultList();
		for (Subject s : subjects) {
			//s.setFlags(new ArrayList<Flag>());
			
		}
		return subjects;

	}

	public SubjectBean getSubjectBean(long id, int commentStart, int length) {
		Subject s = getSubjectById(id);
		SubjectBean bean = new SubjectBean(s);
		return bean;
	}

	public int countSubjects() {
		Query query = em.createQuery("Select s from Subject s");
		List<Subject> subjects = query.getResultList();
		return subjects.size();
	}

	/**
	 * @param s
	 *            an existing subject
	 * @param c
	 *            the new comment
	 * @return the new comment id;
	 */
	public String addComment(Subject s, Comment c) {
		
		
		em.persist(c);
		System.out.println("Persisted comment "+c.getContent());
		em.merge(s);
		s.getComments().add(c);
		return c.getId();
	}

}
