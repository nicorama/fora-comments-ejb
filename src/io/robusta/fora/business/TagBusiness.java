package io.robusta.fora.business;

import io.robusta.fora.domain.Comment;
import io.robusta.fora.domain.Tag;

import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class TagBusiness {

	@PersistenceContext
	EntityManager em;
	
	@EJB
	CommentBusiness commentBusiness;
	
	private final static Logger logger = Logger.getLogger(TagBusiness.class
			.getName());

	
	public Tag getOrCreateTag(String tagName){
		Tag tag = em.find(Tag.class, tagName);
		if (tag == null){
			logger.info("Creating new Tag : " +tagName);
			tag = new Tag(tagName);
			em.persist(tag);
		}
		return tag;
	}
	
	
	public void tagComment(String commentId, String tagName) {

		logger.info("tagging comment " + commentId + " with " + tagName);
		Comment comment = commentBusiness.getCommentById(commentId);
		
		Tag tag = this.getOrCreateTag(tagName);
		comment.getTags().add(tag);
	}
	
	
	
	

}
