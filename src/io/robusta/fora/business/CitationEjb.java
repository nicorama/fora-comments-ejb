package io.robusta.fora.business;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.faces.bean.SessionScoped;
import javax.inject.Named;

//@Stateless
//@SecurityDomain("ForaRealm")
@Stateful
public class CitationEjb {

	@Resource
	SessionContext sessionContext;

	/*@RolesAllowed("user")
	public String getUserCitation() {
		String user = sessionContext.getCallerPrincipal().getName();
		return "Greetings from " + user;
	}*/
	
	int count=0;
	
	public CitationEjb() {
		System.out.println("I'm creating the bean "+this);
	}
	
	String citation="hello";
	
	public String getCitation(){
		return citation + " - "+ count++;
	}

}
