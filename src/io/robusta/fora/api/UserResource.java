package io.robusta.fora.api;

import io.robusta.fora.business.UserBusiness;
import io.robusta.fora.domain.User;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;


@Path("users")
public class UserResource {

	@EJB
	UserBusiness userBusiness;
	

	@Path("test")
	@GET
	public String test(){
		return "test";
	}
	
	@GET
	@Path("hello")
	public String helloAdmin(@QueryParam("admin") boolean isAdmin) {
		return "isAdmin ? " + isAdmin;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)	
	public List<User> getUsers(){
		return userBusiness.findAll();		
	}
	
	
	
}
