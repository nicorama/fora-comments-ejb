package io.robusta.fora.api;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("api/0.1/")
public class ApiApplication extends Application{

}
