package io.robusta.fora.domain;

import io.robusta.util.DebugLoginModule;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.jboss.security.auth.spi.Util;

@Entity
public class Account implements Serializable {

	
	private static final long serialVersionUID = 7350626890969597578L;
	public static final String USER="user", ADMIN="admin", SUPERUSER="superuser";
		
		
	
	public Account() {	
	}
	
	public Account(String email, String password, String role) {
		this.email = email;
		this.clearPassword = password;
		this.password = createHashedPassword(email, password);
		this.role = role;
	}
	
	public String createHashedPassword(String username, String password){
		return Util.createPasswordHash("SHA", Util.BASE64_ENCODING, "UTF-8", username, password);
	}
	
	@Id
	@GeneratedValue
	long id;
	String email;
	String password;
	String clearPassword;
	String role;
	
}

