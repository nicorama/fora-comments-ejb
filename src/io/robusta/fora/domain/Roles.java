package io.robusta.fora.domain;

import javax.persistence.Entity;


public enum Roles {

	USER, ADMIN, SUPERADMIN;
	
	public String toString() {
		
		return this.name().toLowerCase();
		
	};
}
