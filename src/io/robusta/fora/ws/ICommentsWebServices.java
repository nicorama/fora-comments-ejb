package io.robusta.fora.ws;

import io.robusta.fora.domain.Comment;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface ICommentsWebServices {

	@WebMethod
	public List<Comment> getComments();
}
