package io.robusta.fora.ws;

import io.robusta.fora.business.CommentBusiness;
import io.robusta.fora.domain.Comment;

import java.util.List;

import javax.ejb.EJB;
import javax.jws.WebService;

@WebService(endpointInterface = "io.robusta.fora.ws.ICommentsWebServices", serviceName = "CommentsWeb")
public class CommentsWebServiceImpl implements ICommentsWebServices{

	@EJB
	CommentBusiness commentBusiness;
	
	@Override
	//@WebMethod
	public List<Comment> getComments() {
		
		return commentBusiness.findAll();
	}

	
	
	
}
