package io.robusta.fora.beans;

import io.robusta.fora.business.CitationEjb;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;


@Named
@SessionScoped
public class CitationBean implements Serializable{

	
	private static final long serialVersionUID = -5058600154652674138L;

	@EJB
	CitationEjb citationEjb;
	
	//i is depending here on the created session because it's a session bean
	int i=0;

	public String getCitation(){
		
		try {
			return citationEjb.getCitation() + "/bean : "+i++;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "fail";
	}
	
	/*public String getUserCitation(){
		
		try {
			return citationEjb.getUserCitation() + "/bean : "+i++;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "fail";
	}*/


}
