package io.robusta.fora.beans;

import io.robusta.fora.business.SubjectBusiness;
import io.robusta.fora.domain.Comment;
import io.robusta.fora.domain.Subject;

import javax.ejb.EJB;
import javax.inject.Named;

@Named
public class CreateCommentBean {

	String email;
	String content;
	@EJB SubjectBusiness ejb;
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public void setContent(String content) {
		System.out.println("getting content");
		this.content = content;
	}
	
	
	public String getEmail() {
		return email;
	}
	
	public String getContent() {
		return content;
	}
	
	public void createComment(){
		Subject s = new Subject();
		s.setId(5);
		Comment c = new Comment(this.getEmail(), this.getContent());
		ejb.addComment(s, c);
		System.out.println("My comment has id : "+c.getId());
	}
}
