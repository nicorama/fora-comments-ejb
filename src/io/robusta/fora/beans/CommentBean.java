package io.robusta.fora.beans;

import javax.xml.bind.annotation.XmlRootElement;

import io.robusta.fora.domain.Comment;
import io.robusta.fora.domain.User;

@XmlRootElement
public class CommentBean {

	String id;
	String content;
	User user = null;
	boolean anonymous = true;
	int flagsCount = 0;
	int score;
	
	public CommentBean(Comment c) {
		this.id = c.getId();
		this.content = c.getContent();
		this.user = c.getUser();
		this.anonymous = c.isAnonymous();
		this.score = c.getScore();
				
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public boolean isAnonymous() {
		return anonymous;
	}
	public void setAnonymous(boolean anonymous) {
		this.anonymous = anonymous;
	}
	public int getFlagsCount() {
		return flagsCount;
	}
	public void setFlagsCount(int flagsCount) {
		this.flagsCount = flagsCount;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	
	
}
