package io.robusta.fora.beans;

import io.robusta.fora.domain.Comment;
import io.robusta.fora.domain.Subject;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SubjectBean {

	
	
	int commentCount;
	String title;	
	
	List<CommentBean> comments = new ArrayList<CommentBean>();

	public SubjectBean(Subject subject) {
		this.title=subject.getTitle();
		this.commentCount = subject.getComments().size();
		for (Comment c : subject.getComments()){
			System.out.println("ADDING COMMENT");
			comments.add(new CommentBean(c));
		}
		
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public int getCommentCount() {
		return commentCount;
	}

	public void setCommentCount(int commentCount) {
		this.commentCount = commentCount;
	}

	public List<CommentBean> getComments() {
		return comments;
	}
	
	public void setComments(List<CommentBean> comments) {
		this.comments = comments;
	}

}
